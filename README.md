# TuzyaLang

### Syntax
```
<Syntax> ::= (<Spaces> <State> <Spaces>)*  
<State>  ::= "print" <Space> <Spaces> <Expr> <Spaces> ";"  
             | <Ident> <Spaces> "=" <Spaces> ("?" | <Expr>) <Spaces> ";"  
             | "if" <Spaces> "(" <Spaces> <Expr> <Spaces> ")" <Spaces> "{" <Syntax> "}" (<Spaces> "else" <Spaces> "{" <Syntax> "}")?  
             | "while" <Spaces> "(" <Spaces> <Expr> <Spaces> ")" <Spaces> "{" <Syntax> "}"  
             | <Expr> ";"  
<Expr>   ::= <Term> (<Spaces> ("+" | "-") <Spaces> <Term>)*  
<Term>   ::= <Factor> (<Spaces> ("*" | "/" | "%") <Spaces> <Factor>)*  
<Factor> ::= <Value> | "(" <Spaces> <Expr> <Spaces> ")"  
<Value>  ::= <Number> | <Ident>  
<Number> ::= <Digit>+  
<Ident>  ::= <Letter> (<Digit> | <Letter>)*  
<Digit>  ::= [0-9]  
<Letter> ::= [A-Z] | [a-z] | "_"  
<Space>  ::= (" " | "\t" | "\n")  
<Spaces> ::= <Space>*  
```
