#ifndef TUZYALANG_HPP_INCLUDED
#define TUZYALANG_HPP_INCLUDED

#include <iostream>
#include <optional>
#include <stdexcept>


namespace tuzyalang
{
typedef int TuzyaInt;

class unexpected_nullopt : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};

class unexpected_nullptr : public std::logic_error
{
    using std::logic_error::logic_error;
};

class invalid_operation : public std::logic_error
{
    using std::logic_error::logic_error;
};

enum class binop_type
{
    ADD = 0,
    SUB = 1,
    MUL = 2,
    DIV = 3,
    MOD = 4
};

class node
{
public:
    node() = default;
    virtual std::optional<TuzyaInt> eval() const = 0;
    virtual ~node() = default;
};

class number : public node
{
    TuzyaInt value_;

public:
    number(TuzyaInt value) : value_{value} {}
    virtual std::optional<TuzyaInt> eval() const override;
    virtual ~number() override = default;
};

class binary_operation : public node
{
    binop_type type_;
    const node *left_;
    const node *right_;

public:
    binary_operation(binop_type type, const node *left, const node *right);
    virtual std::optional<TuzyaInt> eval() const override;
    virtual ~binary_operation() override;
};

class block : public node
{
    const node *first_;
    const node *second_ = nullptr;

public:
    block(const node *first, const node *second);
    virtual std::optional<TuzyaInt> eval() const override;
    virtual ~block() override;
};

class print : public node
{
    const node *expression_;

public:
    print(const node *expression);
    virtual std::optional<TuzyaInt> eval() const override;
    virtual ~print() override;
};

class conditional_statement : public node
{
    const node *condition_;
    const node *then_branch_;
    const node *else_branch_;

public:
    conditional_statement(const node *condition, const node *then_branch, const node *else_branch = nullptr);
    virtual std::optional<TuzyaInt> eval() const override;
    virtual ~conditional_statement() override;
};

class syntax : public node
{
    const node *root_statement_ = nullptr;

public:
    syntax(const node *root_statement);
    virtual std::optional<TuzyaInt> eval() const override;
    virtual ~syntax() override;
};

class program
{
    std::string source_;
    const syntax *ast = nullptr;

public:
    program(const std::string &source) : source_{source} {}

    void parse()
    {
        ast = parse_syntax();
    }

    ~program()
    {
        delete ast;
    }
};
};

#endif // TUZYALANG_HPP_INCLUDED
