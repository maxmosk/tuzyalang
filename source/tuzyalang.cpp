#include "tuzyalang.hpp"

namespace tuzyalang
{
// ================================ number ================================
std::optional<TuzyaInt> number::eval() const
{
    return value_;
}

// ================================ binary_operation ================================
binary_operation::binary_operation(binop_type type, const node *left, const node *right)
        : type_{type}, left_{left}, right_{right}
{
    if (nullptr == left_)
    {
        throw unexpected_nullptr("Left operand of binapy operation node constructor can not be nullptr");
    }
    if (nullptr == right_)
    {
        throw unexpected_nullptr("Right operand of binapy operation node constructor can not be nullptr");
    }
}

std::optional<TuzyaInt> binary_operation::eval() const
{
    auto left_val = left_->eval();
    auto right_val = right_->eval();
        
    if (std::nullopt == left_val)
    {
        throw unexpected_nullopt("Null-value left expression is non-operatable");
    }
    if (std::nullopt == right_val)
    {
        throw unexpected_nullopt("Null-value right expression is non-operatable");
    }

    switch (type_)
    {
    case binop_type::ADD:
        return *left_val + *right_val;
        break;
    case binop_type::SUB:
        return *left_val - *right_val;
        break;
    case binop_type::MUL:
        return *left_val * *right_val;
        break;
    case binop_type::DIV:
        return *left_val / *right_val;
        break;
    case binop_type::MOD:
        return *left_val % *right_val;
        break;
    
    default:
        throw invalid_operation("Unexpected binary operation type");
        break;
    }
}

binary_operation::~binary_operation()
{
    delete left_;
    delete right_;
}

// ================================ block ================================
block::block(const node *first, const node *second) : first_{first}, second_{second}
{
    if (nullptr == first_)
    {
        throw unexpected_nullptr("First argument of block node constructor can not be nullptr");
    }
}

std::optional<TuzyaInt> block::eval() const
{
    first_->eval();
    if (nullptr != second_)
    {
        second_->eval();
    }

    return std::nullopt;
}

block::~block()
{
    delete first_;
    delete second_;
}

// ================================ print ================================
print::print(const node *expression) : expression_{expression}
{
    if (nullptr == expression_)
    {
        throw unexpected_nullptr("Expression in print node constructor can not be nullopt");
    }
}

std::optional<TuzyaInt> print::eval() const
{
    auto expr_val = expression_->eval();
    if (std::nullopt != expr_val)
    {
        std::cout << *expr_val << std::endl;
    }
    else
    {
        throw unexpected_nullopt("Null-value expression is unprintable");
    }

    return std::nullopt;
}

print::~print()
{
    delete expression_;
}

// ================================ conditional_statement ================================
conditional_statement::conditional_statement(const node *condition, const node *then_branch, const node *else_branch)
            : condition_{condition}, then_branch_{then_branch}, else_branch_{else_branch}
{
    if (nullptr == condition_)
    {
        throw unexpected_nullptr("Condition in conditional statrement node constructor can not be nullptr");
    }
    if (nullptr == then_branch_)
    {
        throw unexpected_nullptr("Then branch in conditional statrement node constructor can not be nullptr");
    }
}

std::optional<TuzyaInt> conditional_statement::eval() const
{
    auto cond = condition_->eval();
    if (std::nullopt == cond)
    {
        throw unexpected_nullopt("Null-value expression is non-boolable");
    }
        
    if (0 != *cond)
    {
        then_branch_->eval();
    }
    else
    {
        else_branch_->eval();
    }

    return std::nullopt;
}

conditional_statement::~conditional_statement()
{
    delete condition_;
    delete then_branch_;
    delete else_branch_;
}

// ================================ syntax ================================
syntax::syntax(const node *root_statement) : root_statement_{root_statement}
{
    ;
}

std::optional<TuzyaInt> syntax::eval() const
{
    root_statement_->eval();
        
    return std::nullopt;
}

syntax::~syntax()
{
    delete root_statement_;
}
};
