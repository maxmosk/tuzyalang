#include "tuzyalang.hpp"
#include <cstdlib>
#include <iostream>

int main()
{
    using namespace tuzyalang;

    syntax program{new print{new binary_operation{binop_type::ADD, new number{3}, new number{2}}}};
    program.eval();
    return EXIT_SUCCESS;
}
